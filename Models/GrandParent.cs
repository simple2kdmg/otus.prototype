﻿using System;
using OTUS.prototype.Interfaces;

namespace OTUS.prototype.Models
{
    public class GrandParent : IMyCloneable<GrandParent>, ICloneable
    {
        public string FullName { get; set; }
        public int Age { get; }
        protected byte Energy;

        public GrandParent(string fullName, int age)
        {
            FullName = fullName;
            Age = age;
            Energy = 2;
        }

        private GrandParent(GrandParent instance) : this(instance.FullName, instance.Age)
        {
            Energy = instance.Energy;
        }

        public virtual GrandParent MyClone()
        {
            return new GrandParent(this);
        }

        public override string ToString()
        {
            return $"GrandParent FullName: {FullName}, Age: {Age}, Energy: {Energy}.{Environment.NewLine}";
        }

        public object Clone()
        {
            return MyClone();
        }
    }
}