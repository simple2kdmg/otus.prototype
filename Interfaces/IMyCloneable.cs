﻿using OTUS.prototype.Models;

namespace OTUS.prototype.Interfaces
{
    public interface IMyCloneable<out T>
    {
        public T MyClone();
    }
}